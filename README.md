
# Udacity Project 06
The objective of this project was to use Jasmine to run tests
<ul>
 <li>Return the allFeeds variable to a passing state.</li>
 <li>Write a test that loops through each feed in the allFeeds object and ensures it has a URL defined and that the URL is not empty.</li>
 <li>Write a test that loops through each feed in the allFeeds object and ensures it has a name defined and that the name is not empty.</li>
 <li>Write a test that ensures the menu element is hidden by default.</li>
 <li>Write a test that ensures the menu changes visibility when the menu icon is clicked. This test should have two expectations: does the menu display when clicked and does it hide when clicked again.</li>
 <li>Write a test that ensures when the loadFeed function is called and completes its work, there is at least a single entry</li>
 <li>Write a test that ensures when a new feed is loaded by the loadFeed function that the content actually changes. Remember, loadFeed() is asynchronous.</li>
</ul>

## How to start
Run the index.html file in your favorite browser and all test should pass.

# Added two extra tests
Added a tes to check if there are menu items.
Added a second extra test to test when a menu item is clicked that there feeds get loaded.

### Things that have changed
<ul>
 <li>Added tests that I inadvertantly removed</li>
 <li>Moved the done function so the second test will run allowing the second variable to register</li>
<li>Please keep in mind that if you remove the .menu-hidden from the body in the html, the .hidden-menu class still shows up.
To test this, remove the .menu-hidden class and comment out line 119 in app.js that reads $('body').addClass('menu-hidden'). This will then render the test false.
</li>
</ul>
