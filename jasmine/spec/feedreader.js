/* feedreader.js
 *
 * This is the spec file that Jasmine will read and contains
 * all of the tests that will be run against your application.
 */

$(function () {
    describe('RSS Feeds', function () {
        // This tests to make sure that the allFeeds variable has been defined and that it is not empty
        it('are defined', function () {
            expect(allFeeds).toBeDefined();
            expect(allFeeds.length).not.toBe(0);
        });

        //This test makes sure that the URL is not empty
        it('URL is not empty', function () {
            //Goes through all the allFeeds and checks to see if there is a URL
            for (var i = 0, len = allFeeds.length; i < len; i++) {
                expect(allFeeds[i].url.length).toBeGreaterThan(0);
                expect(allFeeds[i].url).toBeDefined();
            }
        });

        //This test iterates through all the allFeeds and check to see if each has a name
        it('Name is not empty', function () {
            for (var i = 0, len = allFeeds.length; i < len; i++) {
                expect(allFeeds[i].name.length).toBeGreaterThan(0);
                expect(allFeeds[i].name).toBeDefined();
            }
        });
    });


    describe('The Menu', function () {
        // This test ensures the menu element is hidden by default
        it('Menu hidden by default', function () {
            //Checks to see if the body class has the menu-hidden class
            expect($('body').hasClass('menu-hidden')).toBeTruthy();
        });

        // This test ensures the menu changes visibility when the menu icon is clicked
        it("Check the menu toggle visibility when icon clicked", function () {
            //Setting up a link variable to use later for a trigger
            var link = $('.menu-icon-link');
            //Set up a body variable to to use later for tests
            var body = $('body');

            //Trigger a link click before the test
            link.trigger('click');
            //Checks to see that the body does not have the menu-hidden class after first click
            expect(body.hasClass('menu-hidden')).toBeFalsy();

            //Trigger a link click before the test
            link.trigger('click');
            //Checks to see that the body has the menu-hidden class after second click
            expect(body.hasClass('menu-hidden')).toBeTruthy();
        });
    });

    describe('Initial Entries', function () {
        //This test ensures when the loadFeed function is called and completes its work, there is at least a single entry
        beforeEach(function (done) {
            loadFeed(0, done);
        });

        it('Has single entry', function () {
            var feedLength = $('.feed .entry').length;
            expect(feedLength).toBeTruthy();
        });
    });

    describe("New Feed Selection", function () {
        // This test ensures when a new feed is loaded by the loadFeed function that the content actually changes.

        //Set the variables for firstFeed and secondFeed for use later
        var firstFeedsContent = {};
        var secondFeedsContent = {};

        beforeEach(function (done) {
            //Check that there are at least two feeds to test against
            expect(allFeeds.length > 1).toBeTruthy();
            //Load the first feed
            loadFeed(0, function () {
                //Set the first feeds title
                firstFeedsContent.title = $('.header-title').text();
                //Set the first feeds text
                firstFeedsContent.text = $('.feed .entry').text();

                //Load the second feed
                loadFeed(1, function () {
                    //Set the second feeds title
                    secondFeedsContent.title = $('.header-title').text();
                    //Set the second feeds text
                    secondFeedsContent.text = $('.feed .entry').text();
                    done();
                });
            });


        });

        it("Changes content", function () {
            //Check that the tile for first content is different from the second feeds title
            expect(firstFeedsContent.title).not.toBe(secondFeedsContent.title);
            //Check that the text for first content is different from the second feeds text
            expect(firstFeedsContent.text).not.toBe(secondFeedsContent.text);
        });

    });

    //Write a test to see if the meny has links
    describe("Menu has links", function () {
        //Set menu item count to 0
        var menuItemCount = 0;
        //Run loadFeed and set the menuItemCount after it is finished
        beforeEach(function (done) {
            loadFeed(0, function () {
                menuItemCount = $('.feed-list li').length;
                done();
            });
        });
        //Test to see that the menu list is populated
        it("Checks that there are links in the menu", function () {
            expect(menuItemCount).toBeGreaterThan(0);
        });
    });

    //write a test to see if feed loads after a menu item is clicked
    describe("Feed items load when menu item clicked", function () {
        var menuItems = $('.feed-list li a');
        //Load the feed before the test
        beforeEach(function (done) {
            loadFeed(0, function () {
                done();
            });
        });
        //Trigger a click on the last menu item
        menuItems.last().trigger('click');
        //Test passes when there is at least one feed
        it("Checks that there are feeds after click", function () {
            expect($('.feed .entry-link').length).toBeGreaterThan(0);
        });
    });
}());
